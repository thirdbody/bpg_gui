import Cocoa
import Foundation

func simpleAlert(message: String) {
    let alert = NSAlert()
    alert.messageText = message
    alert.alertStyle = .warning
    alert.addButton(withTitle: "OK")

    _ = alert.runModal()

    alert.window.close()
    NSApp.activate(ignoringOtherApps: true)
}
