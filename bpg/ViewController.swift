import Cocoa

var bpgDebug = false

class ViewController: NSViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Better Privacy Guard"
    }

    // The menu item "Debug -> Enabled" is attached to this function through the First Responder.
    @IBAction func toggleBpgDebug(_: NSMenuItem) {
        NSLog("Debug mode is now ON.")
        bpgDebug = !bpgDebug
    }

    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
}
