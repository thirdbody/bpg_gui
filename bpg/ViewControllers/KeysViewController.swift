import Cocoa

class KeysViewController: NSViewController {
    @IBOutlet var keysList: NSPopUpButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        title = "Keys"
        keysList.removeAllItems()
        keysList.addItems(withTitles: getKeys())
    }
}
