import Cocoa

class DecryptionViewController: NSViewController {
    @IBOutlet var encryptedMessageField: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        title = "Decrypt"
    }

    @IBAction func decryptFromClipboard(_: Any) {
        let msg = NSPasteboard.general.pasteboardItems?.first?.string(forType: .string)

        encryptedMessageField.stringValue = decrypt(message: msg!);
    }

    @IBAction func manualDecrypt(_: Any) {
        encryptedMessageField.stringValue = decrypt(message: encryptedMessageField.stringValue)
    }
}
